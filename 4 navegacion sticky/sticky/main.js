(function(){
  let pinged = false; //para saber si ya ha sido pegada la navegacion a la parte de arriba
  let nav = document.querySelector(".nav");
  let coords = nav.getBoundingClientRect();//nos da la altura,anchura y distancia que tiene el elemento con las esquinas
  let stickyScrollPoint = coords.top;

  function pingToTop(){
    if(pinged) return;

    nav.classList.add("pined");

    pinged = true;
  }

  function unPingFromTop(){
    console.log(pinged);
    if(!pinged) return;

    nav.classList.remove("pined");

    pinged = false;
  }

  window.addEventListener('scroll',function(ev){
    if(window.scrollY < stickyScrollPoint) return unPingFromTop();
    let coords = nav.getBoundingClientRect();
    if(coords.top <= 0) return pingToTop();

    unPingFromTop();
  })

})();
